package de.rnholzinger01.engine.file;

import java.io.File;

/**
 * 
 * Used to manage the content of a specific folder in the computer's APPDATA folder
 * 
 * @author rnholzinger01
 * @since 20.10.2017 14:32
 * @version 1.0.0
 *
 */
public class FileManager {

	/**
	 * Folder in APPDATA which should be managed by this instance of {@link FileManager}
	 */
	private final File folder;

	/**
	 * Used that is used as prefix in notification massages
	 */
	public final String symbol = "FileManager";

	/**
	 * Constructor: Declares the folder where the {@link FileManager} should work in
	 * @param folderName name of the folder in the computer's APPDATA folder
	 */
	public FileManager(String folderName) {
		String folder = System.getenv("APPDATA") + "\\" + folderName;

		String os = System.getProperty("os.name").toUpperCase();
		if (os.contains("WIN")) {
			folder = System.getenv("APPDATA") + "\\" + folderName;
			System.out.println("[" + symbol + "] Found windows APPDATA folder");
		}
		if (os.contains("MAC")) {
			folder = System.getProperty("user.home") + "/Library/Application " + "Support"
					+ folderName;
			System.out.println("[" + symbol + "] Found mac Library folder");
		}
		if (os.contains("NUX")) {
			folder = System.getProperty("user.dir") + "." + folderName;
			System.out.println("[" + symbol + "] Found linux dir folder");
		}

		File dir = new File(folder);

		if (dir.exists()) {
			System.out.println("[" + symbol + "] Found " + folderName + " (managed folder)");
		} else {
			dir.mkdir();
			System.out.println("[" + symbol + "] Couldn't find " + folderName + " (managed folder) so created it");
		}

		this.folder = dir;
	}

	public File getFolder() {
		return folder;
	}
	
}