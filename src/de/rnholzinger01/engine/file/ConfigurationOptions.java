package de.rnholzinger01.engine.file;

import java.util.HashMap;
import java.util.Map;

/**
 * 
 * @author rnholzinger01
 * @since 22.10.2017 15:00
 * @version 1.0.0
 * 
 */
@SuppressWarnings("rawtypes")
public class ConfigurationOptions {

	private Map options = new HashMap<String, Object>();
	/**
	 * TODO: Implement defaults
	 */
	@SuppressWarnings("unused")
	private Map defaults = new HashMap<String, Object>();

	public ConfigurationOptions(Map map) {
		if(map == null) {
			throw new NullPointerException("The new map is empty");
		}
		this.options = map;
	}

	public void updateMap(Map map) {
		if(map == null) {
			throw new NullPointerException("The new map is empty");
		}
		options = map;
	}

	public Map getMap() {
		return options;
	}

	public Object getValue(String key) {
		Map newMap = options;
		while(key.contains(".")) {
			String left = key.substring(0, key.indexOf('.'));
			key = key.substring(key.indexOf('.')+1);
			newMap = (Map) newMap.get(left);
		}

		return newMap.get(key);
	}

	@SuppressWarnings("unchecked")
	public void setValue(String key, Object value) {
		System.out.println(options);

		Map newMap = options;
		while(key.contains(".")) {
			System.out.println("1_key: " + key);
			System.out.println("1_map: " + newMap);
			String left = key.substring(0, key.indexOf('.'));
			key = key.substring(key.indexOf('.')+1);
			newMap = (Map) newMap.get(left);
			System.out.println("2_key: " + key);
			System.out.println("2_map: " + newMap);
		}

		newMap.put(key, value);
	}

	/**
	 * @deprecated NOT FINISHED!!!!!
	 */
	public void addDefaults(Map map) {

	}
	
	/**
	 * @deprecated NOT FINISHED!!!!!
	 */
	public void setDefaults(Map map) {

	}
	
	/**
	 * @deprecated NOT FINISHED!!!!!
	 */
	public void addDefault(Map map) {

	}
}
	
