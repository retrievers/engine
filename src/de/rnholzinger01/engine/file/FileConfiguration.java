package de.rnholzinger01.engine.file;

import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import com.esotericsoftware.yamlbeans.YamlReader;
import com.esotericsoftware.yamlbeans.YamlWriter;

/**
 * 
 * Used to store data in a file
 * <br><br>
 * TODO: Replace System.err.println with own Error<br>
 * TODO: finish JavaDoc
 * <br><br>
 * Syntax for {@link FileConfiguration}:<br>
 * - each line: "key: value" (space after ':' needed)<br>
 * - spaces or tabulators are only allow, if value is a string<br>
 * - strings should be declared with " " or ' '
 * 
 * @author rnholzinger01
 * @since 20.10.2017 15:11
 * @version 1.0.0
 * 
 */
@SuppressWarnings("rawtypes")
public class FileConfiguration {

	private FileManager fm;
	private String relPath, fileName;
	private ConfigurationOptions options;
	private String name;

	public FileConfiguration(FileManager fm, String relPath, String fileName) {
		this.fm = fm;
		this.relPath = relPath;
		this.fileName = fileName;
		this.name = fileName;
	}

	/**
	 * Used to load a {@link FileConfiguration} out of a {@link File} and put its data into the <b>entries</b> {@link HashMap}
	 * @param relPath folder name in {@link FileManager} directory
	 * @param fileName name of the configuration file
	 * @throws IOException
	 */
	public void loadConfiguration() throws IOException {
		File dir = new File(fm.getFolder().getAbsolutePath() + "\\" + relPath);

		//Create the configuration's folder if it doesn't exist
		if(!dir.exists()) {
			dir.mkdirs();
		}

		File file = new File(dir, fileName + ".yml");

		//Create the configuration file if it doesn't exist
		if(!file.exists()) {
			System.out.println("[" + fm.symbol + "] Couldn't find " + fileName + ".yml" + " so created it");
			file.createNewFile();
			file = new File(dir, fileName + ".yml"); //maybe useless
		}


		FileReader fr = new FileReader(fm.getFolder() + "\\" + relPath + "\\" + fileName + ".yml");
		YamlReader yr = new YamlReader(fr);
		
		options = new ConfigurationOptions((Map) yr.read());
	}

	/*
	 * Save
	 */
	/**
	 * Saves the {@link #options()} to the {@link File}
	 * @throws IOException
	 */
	public void save() throws IOException {
		Map map = options.getMap();
		YamlWriter writer = new YamlWriter(new FileWriter(fm.getFolder().getAbsolutePath() + "\\" + relPath + "\\" + fileName + ".yml"));
		writer.write((Object) map);
		writer.close();
	}

	/**
	 * @return the {@link ConfigurationOptions} of this {@link FileConfiguration}
	 */
	public ConfigurationOptions options() {
		return options;
	}

	/**
	 * @return the name of the {@link FileConfiguration} as {@link String}
	 */
	public String getName() {
		return name;
	}

	/**
	 * @return all keys with their values in a {@link HashMap}
	 */
	public Map getAll() {
		return options.getMap();
	}

	/**
	 * Used to set keys with their values
	 * @param entries in a {@link HashMap}
	 */
	public void setAll(Map entries) {
		this.options.updateMap(entries);
	}

	/**
	 * @param key for the returned value as {@link String}
	 * @return the value for the key in the parameters as {@link String}
	 * @attention the value able to be casted to {@link String}
	 */
	public String getString(String key) {
		return (String) options.getValue(key);
	}

	/**
	 * @param key for the returned value as {@link String}
	 * @return true: if value of key in entries is true<br>
	 * 		   false: if value of key in entries NOT is true
	 * @attention the value able to be casted to {@link Boolean}
	 */
	public boolean getBoolean(String key) {
		if((boolean) options.getValue(key)) {
			return true;
		} else {
			return false;
		}
	}

	/**
	 * @param key for the returned value as {@link String}
	 * @return the value for the key in the parameters as {@link Integer}
	 * @attention the value able to be casted to {@link Integer}
	 */
	public int getInt(String key) {
		return (int) options.getValue(key);
	}

	/**
	 * @param key for the returned value as {@link String}
	 * @return the value for the key in the parameters as {@link Long}
	 * @attention the value able to be casted to {@link Long}
	 */
	public long getLong(String key) {
		return (long) options.getValue(key);
	}

	/**
	 * @param key for the returned value as {@link String}
	 * @return the value for the key in the parameters as {@link Double}
	 * @attention the value able to be casted to {@link Double}
	 */
	public double getDouble(String key) {
		return (double) options.getValue(key);
	}

	/**
	 * @param key for the returned value as {@link String}
	 * @return the value for the key in the parameters as {@link Float}
	 * @attention the value able to be casted to {@link Float}
	 */
	public float getFloat(String key) {
		return (float) options.getValue(key);
	}


	/*
	 * Setters
	 */
	public void set(String key, Object value) {
		options.setValue(key, value);
	}

	/*
	 * Defaults
	 */
	/**
	 * TODO: Defaults
	 */

}