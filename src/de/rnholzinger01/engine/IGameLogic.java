package de.rnholzinger01.engine;

import de.rnholzinger01.engine.render.Frame;

/**
 * 
 * @author rnholzinger01
 * @since 21.10.2017 18:52
 * @version 1.0.0
 * 
 */
public interface IGameLogic {

	void init(Frame frame) throws Exception;
	
    void input(Frame frame);
    
    void tick(float interval);
    
    void render(Frame frame);
    
    void clean();
	
}