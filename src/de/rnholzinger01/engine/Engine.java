package de.rnholzinger01.engine;

import de.rnholzinger01.engine.render.Frame;

/**
 * 
 * Used as GameEngine class with GameLoop etc.
 * 
 * @author rnholzinger01
 * @since 20.10.2017 14:32
 * @version 1.0.0
 *
 */
public class Engine implements Runnable {

	private boolean running;
	private final Frame frame;
	private final Thread thread;
    private final IGameLogic gameLogic;
	
	//Variables for customization
	private short tickRate = 60;
	private short frameRate = 90;
	private short statsRate = 1;
	
	//Variables for statistics
	private int tps;
	private int fps;
	
	public Engine(String threadName, String frameTitle, int width, int height, boolean vSync, IGameLogic gameLogic) {
		thread = new Thread(this, threadName);
		frame = new Frame(frameTitle, width, height, vSync);
		this.gameLogic = gameLogic;
	}

	/**
	 * Starts the {@link Engine} by creating and starting a new {@link Thread}
	 */
	public void start() {
		if(running) return; running = true;
		String osName = System.getProperty("os.name");
	    if ( osName.contains("Mac") ) {
	    	thread.run();
	    } else {
	    	thread.start();
	    }
	}

	/**
	 * Stops the {@link Engine} by terminating the frame and interrupting the main {@link Thread}
	 */
	public void stop() {
		if(!running){
			System.out.println("stop() method in class Engine failed, because game was not running");
		} else {
			thread.interrupt();
			running = false;
		}
	}
	
	private void init() throws Exception {
        frame.init();
        gameLogic.init(frame);
	}


	/**
	 * Executed after starting a {@link Thread} in {@link #start()} method.<br>
	 * Initializes the frame and starts the GameLoop
	 */
	@Override
	public void run() {
		try {
	        init(); //initialize game
	        loop(); //run gameloop until interruption
	    } catch (Exception ex) {
	        ex.printStackTrace();
	    } finally {
	        clean();
	    }
	}

	//GameLoop
	private void loop() {
		System.out.println("[INFO] Starting GameLoop...");
		long last = System.nanoTime(); //time of the last loop's start
		short tr = tickRate; //tickrate: amount of ticks which should be completed in one second
		double nst = 1000000000/tr; //duration of one tick in nanoseconds
		double delta = 0;
		
		short fr = tickRate; //framerate: amount of frames which should be rendered in one second
		double nsf = 1000000000/fr; //duration of one frame in nanoseconds
		
		//following variables are only used for statistics
		long timer = System.currentTimeMillis();
		int ticks = 0; //for counting the ticks in the last second
		int frames = 0; //for counting the frames the last second
		
		while(!frame.frameShouldClose()) {
			input();
			
			long now = System.nanoTime();
			delta = (now-last) / nst;
			last = now;
			
			while(delta >= 1) {
				tick(1/tr);
				tps++;
				delta--;
			}
			
			render();
			frames++;
			
			if((System.currentTimeMillis() - timer) >= 1000*statsRate) { //executed every statsRate second
				timer += 1000;
				this.tps = ticks; ticks=0;
				this.fps = frames; frames=0;
				System.out.println("[INFO] Game running with ~" + tps);
				System.out.println("[INFO] and ~" + fps + " fps");
				tr = this.tickRate;
				fr = this.frameRate;
			}
			sync(now, nsf);
		}
	}
	
	/**
	 * @param start time of iteration start in nanoseconds
	 * @param ns duration of one tick in nanoseconds
	 */
	private void sync(long start, double ns) {
		double end = start + ns; 
		while(System.nanoTime() < end) {
			try {
				Thread.sleep(1);
			} catch (InterruptedException ie) { ie.printStackTrace(); }
		}
	}
	
	//Cleans up the frame
	private void clean(){
		gameLogic.clean();
	}
	
	//Handles the user's input
	private void input() {
		//TODO: mouseInput
		gameLogic.input(frame);
	}

	//Updates the current state
	private void tick(float interval) {
		gameLogic.tick(interval);
	}
	
	//Renders a new frame
	private void render() {
		gameLogic.render(frame);
		frame.refresh();
	}

	/*
	 * Getters & Setters
	 */
	/**
	 * @return true if the game is running<br>
	 * 			false if the game is NOT running
	 */
	public boolean isRunning() {
		return running;
	}

	/**
	 * @return the amount of ticks which the game should handle each second.
	 */
	public short getTickRate() {
		return tickRate;
	}

	/**
	 * Sets the amount of ticks which the game should handle each second
	 * @param tickRate : amount of ticks which the game should handle each second
	 */
	public void setTickRate(short tickRate) {
		this.tickRate = tickRate;
	}

	/**
	 * @return the amount of frames which the game should render each second.
	 */
	public short getFrameRate() {
		return frameRate;
	}

	/**
	 * Sets the amount of frames which the game should render each second.
	 * @param frameRate : amount of frames which the game should render each second.
	 */
	public void setFrameRate(short frameRate) {
		this.frameRate = frameRate;
	}

	/**
	 * @return the time in seconds until the statistics get refreshed.
	 */
	public short getStatsRate() {
		return statsRate;
	}

	/**
	 * Sets the time in seconds until the statistics get refreshed.
	 * @param statsRate : time in seconds until the statistics get refreshed
	 * @values from 1 to positive infinity
	 * @default 1
	 */
	public void setStatsRate(short statsRate) {
		this.statsRate = statsRate;
	}
	
	/**
	 *  @return the amount of ticks/updates in the last completed statistics period.
	 */
	public int getTps() {
		return tps;
	}

	/**
	 *  @return the amount of rendered frames in the last completed statistics period.
	 */
	public int getFps() {
		return fps;
	}
	
}