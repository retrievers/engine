package de.rnholzinger01.engine.render;

import static org.lwjgl.glfw.GLFW.*;
import static org.lwjgl.opengl.GL11.*;

import org.lwjgl.glfw.GLFW;
import org.lwjgl.glfw.GLFWErrorCallback;
import org.lwjgl.glfw.GLFWVidMode;
import org.lwjgl.opengl.GL;

import static org.lwjgl.system.MemoryUtil.NULL;

/**
 * 
 * @author rnholzinger01
 * @since 22.10.2017
 * @version 1.0.0
 * 
 */
public class Frame {

	//Title of the frame
	private final String title;
	
	//Width and height of the frame
	private int width, height;
	
	//ID of the frame as long 9handling by GLFW)
	private long frameHandle;
	
	//Defines if the Frame if resizable or not
	private boolean resizable;
	
	//Defines if vSync if enabled. (vSync: game has same frameRate as the monitor)
	private boolean vSync;

	public Frame(String title, int width, int height, boolean vSync) {
		this.title = title;
		this.width = width;
		this.height = height;
		this.vSync = vSync;
		//TODO: implement resizing
		this.resizable = false;
	}

	public void init() {
		// Setup the default implementation of error callback which will print the error message in System.err.
		GLFWErrorCallback.createPrint(System.err).set();

		// Initialize GLFW. (Most GLFW functions are not functionally before doing this.)
		if (!glfwInit()) {
			throw new IllegalStateException("GLFW initialisation failed");
		}

		/*
		 * GLFW Window Hints
		 */
		glfwDefaultWindowHints(); // not necessary
		glfwWindowHint(GLFW_VISIBLE, GL_FALSE); // the frame will stay hidden after creation
		glfwWindowHint(GLFW_RESIZABLE, GL_TRUE); // the frame will be resizable
		glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 3);
		glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 2);
		glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);
		glfwWindowHint(GLFW_OPENGL_FORWARD_COMPAT, GL_TRUE);

		// Creates the window
		frameHandle = glfwCreateWindow(width, height, title, NULL, NULL);
		if (frameHandle == NULL) {
			throw new RuntimeException("Failed to create the GLFW frame");
		}

		// Setup resize callback
		glfwSetFramebufferSizeCallback(frameHandle, (frame, width, height) -> {
			this.width = width;
			this.height = height;
			this.setResizable(true);
		});

		// Setup a key callback. It will be called every time a key is pressed, repeated or released.
		glfwSetKeyCallback(frameHandle, (frame, key, scancode, action, mods) -> {
			if (key == GLFW_KEY_ESCAPE && action == GLFW_RELEASE) {
				glfwSetWindowShouldClose(frame, true); // We will detect this in the rendering loop
			}
		});

		// Gets the resolution of the primary monitor
		GLFWVidMode vidmode = glfwGetVideoMode(glfwGetPrimaryMonitor());
		// Centers the frame
		glfwSetWindowPos(
				frameHandle,
				(vidmode.width() - width) / 2,
				(vidmode.height() - height) / 2
				);

		// Makes the OpenGL context current
		glfwMakeContextCurrent(frameHandle);

		if (isvSync()) {
			// Enables v-sync
			glfwSwapInterval(1);
		}

		// Makes the frame visible
		glfwShowWindow(frameHandle);

		GL.createCapabilities();

		// Sets the clear color to default black
		glClearColor(0.0f, 0.0f, 0.0f, 0.0f);
	}

	/**
	 * Sets the color which if used for clearing the {@link Frame}
	 * @param r : red part of the clear color
	 * @param g : green part of the clear color
	 * @param b : blue part of the clear color
	 * @param alpha : alpha channel of the clear color
	 */
	public void setClearColor(float r, float g, float b, float alpha) {
		glClearColor(r, g, b, alpha);
	}

	/**
	 * @param keyCode : id of a key as {@link Integer}
	 * @return whether the key in the parameters is pressed or not
	 */
	public boolean isKeyPressed(int keyCode) {
		return glfwGetKey(frameHandle, keyCode) == GLFW_PRESS;
	}

	/**
	 * @return if true, a close request will be sent to {@link GLFW}
	 */
	public boolean frameShouldClose() {
		return glfwWindowShouldClose(frameHandle);
	}

	/**
	 * @return the title of the {@link Frame}
	 */
	public String getTitle() {
		return title;
	}

	/**
	 * @return the width of the {@link Frame}
	 */
	public int getWidth() {
		return width;
	}

	/**
	 * @return the height of the {@link Frame}
	 */
	public int getHeight() {
		return height;
	}

	/**
	 * @return whether the {@link Frame} is resizable or not
	 */
	public boolean isResizable() {
		return resizable;
	}

	/**
	 * @param resized makes the {@link Frame} resizable or not
	 */
	public void setResizable(boolean resized) {
		this.resizable = resized;
	}

	/**
	 * @return whether vsync is enabled or not (vSync: game has same frameRate as the monitor)
	 */
	public boolean isvSync() {
		return vSync;
	}

	/**
	 * @param vSync if true, vsync will be turned on (vSync: game has same frameRate as the monitor)
	 */
	public void setvSync(boolean vSync) {
		this.vSync = vSync;
	}

	/**
	 * Updates the {@link Frame}
	 */
	public void refresh() {
		glfwSwapBuffers(frameHandle);
		glfwPollEvents();
	}

	public long getFrameHandle() {
		return frameHandle;
	}

	public void setFrameHandle(long frameHandle) {
		this.frameHandle = frameHandle;
	}
	
	public void clean() {
		glfwDestroyWindow(frameHandle);
		glfwTerminate();
	}

}
