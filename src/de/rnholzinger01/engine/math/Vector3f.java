package de.rnholzinger01.engine.math;

/**
 * 
 * @author RNHolzinger01
 * @version 1.0.0
 *
 *	NOT TESTED!!!
 *
 */

public class Vector3f {

	float x;
	float y;
	float z;
	
	public Vector3f() {
		this.x = 0.0f;
		this.y = 0.0f;
		this.z = 0.0f;
	}
	
	public Vector3f(float x, float y, float z) {
		this.x = x;
		this.y = y;
		this.z = z;
	}
	
	
	/**
	 * Vector Addition
	 * @param x
	 */
	public void addX(float x){
		this.x += x;
	}
	public void addY(float y){
		this.y += y;
	}
	public void addZ(float z){
		this.z += z;
	}
	public void substractX(float x){
		this.x -= x;
	}
	public void substractY(float y){
		this.y -= y;
	}
	public void substractZ(float z){
		this.z -= z;
	}
	public void add(Vector3f v){
		addX(v.getX());
		addY(v.getY());
		addZ(v.getZ());
	}
	public void substract(Vector3f v){
		substractX(v.getX());
		substractY(v.getY());
		substractZ(v.getZ());
	}

	/**
	 * Vector Multiplication
	 * @param x
	 */
	public void scaleX(float scalar){
		this.x *= scalar;
	}
	public void scaleY(float scalar){
		this.y *= scalar;
	}
	public void scaleZ(float scalar){
		this.z *= scalar;
	}
	public void scale(float scalar){
		scaleX(scalar);
		scaleY(scalar);
		scaleZ(scalar);
	}
	
	/**
	 * Vector Normalization
	 * @param x
	 */
	public void normalize(){
		if(getLenght()==0) return;
		scale(1f/getLenght());
	}

	/**
	 * Vector Rotation
	 * @param x
	 * 
	 * ----------> NOT AVAILABLE
	 * 
	 */
	/*public void rotateBy90(){
		float oldX = x, oldY = y, oldY = z;
		setX(-oldX); setY(oldY);
	}
	public void rotateBy180(){
		scale(-1);
	}
	public void rotateBy270(){
		float oldX = x, oldY = y, oldY = z;
		setX(oldX); setY(-oldY);
	}*/
	
	/**
	 * Getters and Setters
	 * @return
	 */
	public float getX() {
		return x;
	}

	public void setX(float x) {
		this.x = x;
	}

	public float getY() {
		return y;
	}

	public void setY(float y) {
		this.y = y;
	}
	
	public float getZ() {
		return z;
	}

	public void setZ(float z) {
		this.z = z;
	}

	public float getLenght(){
		return (float) Math.sqrt(Math.pow(x, 2.0) + Math.pow(y, 2.0) + Math.pow(z, 2.0));
	}
	
	public float getScalarProduct(Vector3f v){
		return (x*v.x+y*v.y+z*v.z);
	}
	
	/**
	 * @param v
	 * @return angle between the vector and the vector which is transmitted as param
	 */
	public float getAngleBetween(Vector3f v){
		float angle = (float) Math.acos(getScalarProduct(v)/getLenght() / v.getLenght());
		return angle;
	}
	
	public boolean isBefore(Vector3f v){
		if(getScalarProduct(v) >= 0) return true; 
		else return false;
	}
	
	public boolean isBehind(Vector3f v){
		if(getScalarProduct(v) < 0) return true; 
		else return false;
	}
	
	/*public boolean isLeft(Vector3D v){
		Vector3D tmp90 = this; tmp90.rotateBy90();
		if(tmp90.isBefore(v)) return true; 
		else return false;
	}
	public boolean isRight(Vector3D v){
		Vector3D tmp90 = this; tmp90.rotateBy90();
		if(tmp90.isBehind(v)) return true; 
		else return false;
	}*/
	
	//TODO: MISSING KREUZPRODUKT -> https://www.youtube.com/watch?v=b6DdYfk4Lss&list=PL58qjcU5nk8tZ3FItMW_8uq1F3sGJ3WVi&index=12
}
