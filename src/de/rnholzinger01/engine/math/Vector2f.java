package de.rnholzinger01.engine.math;

/**
 * 
 * @author RNHolzinger01
 * @version 1.0.0
 *
 */

public class Vector2f {

	public float x;
	public float y;
	
	public Vector2f() {
		this.x = 0.0f;
		this.y = 0.0f;
	}
	
	public Vector2f(float x, float y) {
		this.x = x;
		this.y = y;
	}
	
	
	/**
	 * Vector Addition
	 * @param x
	 */
	public void addX(float x){
		this.x += x;
	}
	public void addY(float y){
		this.y += y;
	}
	public void substractX(float x){
		this.x -= x;
	}
	public void substractY(float y){
		this.y -= y;
	}
	public void add(Vector2f v){
		addX(v.getX());
		addY(v.getY());
	}
	public void substract(Vector2f v){
		substractX(v.getX());
		substractY(v.getY());
	}

	/**
	 * Vector Multiplication
	 * @param x
	 */
	public void scaleX(float scalar){
		this.x *= scalar;
	}
	public void scaleY(float scalar){
		this.y *= scalar;
	}
	public void scale(float scalar){
		scaleX(scalar);
		scaleY(scalar);
	}
	
	/**
	 * Vector Normalization
	 * @param x
	 */
	public void normalize(){
		if(getLenght()==0) return;
		scale(1f/getLenght());
	}

	/**
	 * Vector Rotation
	 * @param x
	 */
	public void rotateBy90(){
		float oldX = x, oldY = y;
		setX(-oldX); setY(oldY);
	}
	public void rotateBy180(){
		scale(-1);
	}
	public void rotateBy270(){
		float oldX = x, oldY = y;
		setX(oldX); setY(-oldY);
	}
	
	/**
	 * Getters and Setters
	 * @return
	 */
	public float getX() {
		return x;
	}

	public void setX(float x) {
		this.x = x;
	}

	public float getY() {
		return y;
	}

	public void setY(float y) {
		this.y = y;
	}

	public float getLenght(){
		return (float) Math.sqrt(Math.pow(x, 2.0) + Math.pow(y, 2.0));
	}
	
	public float getScalarProduct(Vector2f v){
		return (x*v.x+y*v.y);
	}
	
	/**
	 * @param v
	 * @return angle between the vector and the vector which is transmitted as param
	 */
	public float getAngleBetween(Vector2f v){
		float angle = (float) Math.acos(getScalarProduct(v)/getLenght() / v.getLenght());
		return angle;
	}
	
	public boolean isBefore(Vector2f v){
		if(getScalarProduct(v) >= 0) return true; 
		else return false;
	}
	
	public boolean isBehind(Vector2f v){
		if(getScalarProduct(v) < 0) return true; 
		else return false;
	}
	
	public boolean isLeft(Vector2f v){
		Vector2f tmp90 = this; tmp90.rotateBy90();
		if(tmp90.isBefore(v)) return true; 
		else return false;
	}
	public boolean isRight(Vector2f v){
		Vector2f tmp90 = this; tmp90.rotateBy90();
		if(tmp90.isBehind(v)) return true; 
		else return false;
	}
}
