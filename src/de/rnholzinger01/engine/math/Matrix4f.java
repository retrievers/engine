package de.rnholzinger01.engine.math;

import java.util.Random;

public class Matrix4f {

	/**
	 * [row*4 + column] = major row ordering
	 * [column*4 + row] = column major ordering -> USED IN THIS CASE
	 */
	
	/*
	 * single dimensional array used, because of 
	 *	easier handling in combination with LWJGL
	 */
	final static int SIZE = 4*4;
	float[] elements = new float[SIZE];
	
	public Matrix4f() {

	}

	public static Matrix4f getIdentity(){
		Matrix4f re = new Matrix4f();
		for(int i=0; i<SIZE;i++){
			re.elements[i] = 0;
		}
		
		re.elements[0*4+0] = 1.0f;	
		re.elements[1*4+1] = 1.0f;	
		re.elements[2*4+2] = 1.0f;	
		re.elements[3*4+3] = 1.0f;	
		return re;
	}
	
	public static Matrix4f getRandom(){
		Matrix4f re = new Matrix4f();
		Random r = new Random();
		for(int i=0; i<SIZE;i++){
			re.elements[i] = (float) r.nextInt(10);
		}
		return re;
	}
	
	public void multiply(Matrix4f m){
		Matrix4f mNew = new Matrix4f();
		for(int m1row=0; m1row<4; m1row++){
			for(int m2column=0; m2column<4; m2column++){
				float value = 0;
				for(int crossing=0; crossing<4; crossing++){
					value += getElement(m1row, crossing) * m.getElement(crossing, m2column);
				}
				mNew.setElement(m1row, m2column, value);
			}
		}
		setElements(mNew.getElements());
		mNew = null;
	}
	
	/**
	 * 
	 * @param fovy Field Of View on the Y-axis
	 * @param aspect aspect ratio between width and height of the Frame
	 * @param zNear near clipping plane distance
	 * @param zFar far clipping plane distance
	 */
	public void perspective(float fovy, float aspect, float zNear, float zFar) {
		
	}
	
	// Getter Setter
	
	public float[] getElements() {
		return elements;
	}

	public void setElements(float[] elements) {
		this.elements = elements;
	}

	public static int getSize() {
		return SIZE;
	}

	public float getElement(int oneDimenionalIndex){
		return elements[oneDimenionalIndex];
	}
	
	public float getElement(int row, int column){
		return elements[column * 4 + row];
	}
	
	public void setElement(int oneDimenionalIndex, float value){
		elements[oneDimenionalIndex] = value;
	}
	
	public void setElement(int row, int column, float value){
		elements[column * 4 + row] = value;
	}
	
	public void print(){
		for(int row=0; row<4; row++){
			String out = "";
			for(int column=0; column<4; column++){
				out += getElement(row, column) + " ";
			}
			System.out.println("row " + row + ": " + out);
		}
	}
	
}
