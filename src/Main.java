import de.rnholzinger01.engine.Engine;
import de.rnholzinger01.engine.IGameLogic;

/**
 * 
 * @author rnholzinger01
 * @since 24.10.2017
 * @version 1.0.0
 * 
 */
public class Main {

	public static void main(String[] args) {
//		FileManager filemanager = new FileManager("RNH Engine");
//		FileConfiguration conf = new FileConfiguration(filemanager, "folder", "options");
//		try {
//			conf.loadConfiguration();
//		} catch (IOException e) {
//			e.printStackTrace();
//		}
//		conf.set("running", true);
//		System.out.println(conf.getString("Game.version.name"));
//		try {
//			conf.save();
//		} catch (IOException e) {
//			e.printStackTrace();
//		}


		//Start game
		try {
            boolean vSync = true;
            IGameLogic gameLogic = new TestGame();
            Engine gameEng = new Engine("TestSpielThread", "Testspiel", 600, 480, vSync, gameLogic);
            gameEng.start();
        } catch (Exception excp) {
            excp.printStackTrace();
            System.exit(-1);
        }
	}

}
