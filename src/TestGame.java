import de.rnholzinger01.engine.IGameLogic;
import de.rnholzinger01.engine.render.Frame;
import de.rnholzinger01.engine.render.Rendering;
import static org.lwjgl.glfw.GLFW.GLFW_KEY_DOWN;
import static org.lwjgl.glfw.GLFW.GLFW_KEY_UP;

/**
 * 
 * @author rnholzinger01
 * @version 1.0.1
 * 
 */
public class TestGame implements IGameLogic {

    private int direction = 0;
    private float color = 0.0f;
	Rendering renderer;
	//private GameItem[] gameItems;
			
	TestGame() {
		renderer = new Rendering();
	}

	@Override
	public void init(Frame frame) throws Exception {
		renderer.init();
	}

	@Override
	public void input(Frame frame) {
        if (frame.isKeyPressed(GLFW_KEY_UP)) {
            direction = 1;
        } else if (frame.isKeyPressed(GLFW_KEY_DOWN)) {
            direction = -1;
        } else {
            direction = 0;
        }
	}

	@Override
	public void tick(float interval) {
		color += direction * 0.01f;
        if (color > 1) {
            color = 1.0f;
        } else if (color < 0) {
            color = 0.0f;
        }
	}

	@Override
	public void render(Frame frame) {
		frame.setClearColor(color, color, color, 0.0f);
        renderer.render(frame);
	}

	@Override
	public void clean() {
		renderer.cleanup();
//		for (GameItem gameItem : gameItems) {
//			gameItem.getMesh().cleanUp();
//		}
	}

}
